# ******************************************
# 1. Blacklist - ENCODE DAC blacklist 
# https://www.encodeproject.org/annotations/ENCSR636HFF/
# ******************************************

#hg19 

cd /media/sequentia/synology_office/Rosa/RNAseq/data/hg19

zcat hg19_blacklist_ENCODE.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{print $1,$2,$3,"Blacklist",$4}' > annotation/hg19_blacklist_ENCODE.bed

#hg38 

cd /media/sequentia/synology_office/Rosa/RNAseq/data/hg38

zcat hg38_blacklist_ENCODE.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{print $1,$2,$3,"Blacklist",$4}' > annotation/hg38_blacklist_ENCODE.bed


# ******************************************
# 2. RNA editing sites - DARNED
# https://github.com/aradenbaugh/radia/tree/master/data/hg19/darned
# ******************************************

#hg19 

cd /media/sequentia/synology_office/Rosa/RNAseq/data/hg19

zcat hg19_editingSites_darned.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{print $1,$2,$3,"editingSites_darned"}' > annotation/hg19_editingSites_darned.bed

#hg38

cd /media/sequentia/synology_office/Rosa/RNAseq/data/hg38

zcat hg38_editingSites_darned.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{print $1,$2,$3,"editingSites_darned"}' > annotation/hg38_editingSites_darned.bed

# ******************************************
# 3. RNA editing sites - RADAR
# https://github.com/aradenbaugh/radia/tree/master/data/hg19/radar
# ******************************************

#hg19 

cd /media/sequentia/synology_office/Rosa/RNAseq/data/hg19

zcat hg19_editingSites_radar.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{print $1,$2,$3,"editingSites_radar"}' > annotation/hg19_editingSites_radar.bed

#hg38

cd /media/sequentia/synology_office/Rosa/RNAseq/data/hg38

zcat hg38_editingSites_radar.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{print $1,$2,$3,"editingSites_radar"}' > annotation/hg38_editingSites_radar.bed

# ******************************************
# 4. RepeatMasker
# This track was created by using Arian Smit's RepeatMasker program, which screens DNA sequences for interspersed repeats and low complexity DNA sequences. 
# ******************************************

#hg19 

cd /media/sequentia/synology_office/Rosa/RNAseq/data/hg19/RepeatMasker

#BED
zcat hg19_RepeatMasker.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{print $1,$2,$3,"RepeatMasker",$4}' > ../annotation/hg19_RepeatMasker.bed


#TXT
zcat hg19_RepeatMasker.txt.gz | awk 'BEGIN{FS="\t";OFS="\t"}{if(NR>1){print $1,$6,$7,$8,$11}}' > ../annotation/hg19_RepeatMasker.txt

#hg38

cd /media/sequentia/synology_office/Rosa/RNAseq/data/hg38/RepeatMasker

#BED
zcat hg38_RepeatMasker.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{print $1,$2,$3,"RepeatMasker",$4}' > ../annotation/hg38_RepeatMasker.bed


#TXT
zcat hg38_RepeatMasker.txt.gz | awk 'BEGIN{FS="\t";OFS="\t"}{if(NR>1){print $1,$6,$7,$8,$11}}' > ../annotation/hg38_RepeatMasker.txt
