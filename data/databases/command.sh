# ******************************************
# 1. Blacklist - ENCODE DAC blacklist 
# https://www.encodeproject.org/annotations/ENCSR636HFF/
# ******************************************

#hg19 

cd /media/sequentia/synology_office/Rosa/RNAseq/data/hg19

wget https://www.encodeproject.org/files/ENCFF001TDO/@@download/ENCFF001TDO.bed.gz

mv ENCFF001TDO.bed.gz hg19_blacklist_ENCODE.bed.gz

#hg38 

cd /media/sequentia/synology_office/Rosa/RNAseq/data/hg38

wget https://www.encodeproject.org/files/ENCFF356LFX/@@download/ENCFF356LFX.bed.gz

mv ENCFF356LFX.bed.gz hg38_blacklist_ENCODE.bed.gz


# ******************************************
# 2. RNA editing sites - DARNED
# https://github.com/aradenbaugh/radia/tree/master/data/hg19/darned
# ******************************************

#hg19 ####################################################

cd /media/sequentia/synology_office/Rosa/RNAseq/data/hg19

mkdir -p DARNED

cd /media/sequentia/synology_office/Rosa/RNAseq/data/hg19/DARNED


# 1) Download the Database of RNA Editing (DARNED) file for NCBI37/hg19 from https://darned.ucc.ie/static/downloads/

wget https://darned.ucc.ie/static/downloads/hg19.txt --no-check-certificate

wget https://raw.githubusercontent.com/aradenbaugh/radia/master/data/hg19/darned/darned_tissue_mapping.txt


# 2) Map the long DARNED tissue source text to an abbreviate version using the provided mapping file: darned_tissue_mapping.txt with a command like this:
#    This command first loads the tissue mappings into 'a', then splits the
#    DARNED tissue column by ',' and stores them in 'b'. The leading and 
#    trailing spaces are removed from the elements in 'b' and the abbreviation
#    for the long text is looked up in 'a'.  

awk -F "\t" 'NR==FNR{a[$2]=$1;next}{split($9,b,",")}{gsub(/^[ ]+|[ ]+$/,"",b[1]); text=a[b[1]]; for (i=2; i<=length(b); i++) {gsub(/^[ ]+|[ ]+$/,"",b[i]); text=text","a[b[i]];}}{print $1"\t"$2"\t"text}' darned_tissue_mapping.txt hg19.txt > darned_hg19_mapped.txt

# 3) Parse the data into chromosome specific .bed files with commands like these:  

awk -F '\t' '($1=="1") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chr1_unsorted.bed
awk -F '\t' '($1=="2") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chr2_unsorted.bed
awk -F '\t' '($1=="3") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chr3_unsorted.bed
awk -F '\t' '($1=="4") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chr4_unsorted.bed
awk -F '\t' '($1=="5") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chr5_unsorted.bed
awk -F '\t' '($1=="6") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chr6_unsorted.bed
awk -F '\t' '($1=="7") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chr7_unsorted.bed
awk -F '\t' '($1=="8") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chr8_unsorted.bed
awk -F '\t' '($1=="9") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chr9_unsorted.bed
awk -F '\t' '($1=="10") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chr10_unsorted.bed
awk -F '\t' '($1=="11") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chr11_unsorted.bed
awk -F '\t' '($1=="12") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chr12_unsorted.bed
awk -F '\t' '($1=="13") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chr13_unsorted.bed
awk -F '\t' '($1=="14") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chr14_unsorted.bed
awk -F '\t' '($1=="15") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chr15_unsorted.bed
awk -F '\t' '($1=="16") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chr16_unsorted.bed
awk -F '\t' '($1=="17") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chr17_unsorted.bed
awk -F '\t' '($1=="18") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chr18_unsorted.bed
awk -F '\t' '($1=="19") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chr19_unsorted.bed
awk -F '\t' '($1=="20") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chr20_unsorted.bed
awk -F '\t' '($1=="21") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chr21_unsorted.bed
awk -F '\t' '($1=="22") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chr22_unsorted.bed
awk -F '\t' '($1=="X") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chrX_unsorted.bed
awk -F '\t' '($1=="Y") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chrY_unsorted.bed
awk -F '\t' '($1=="M") {print $1"\t"$2-1"\t"$2"\t"$3}' darned_hg19_mapped.txt  > chrM_unsorted.bed


# 3) sort and gzip the files with commands like these:

sort -t $'\t' +1n -3 chr1_unsorted.bed | gzip > chr1.bed.gz
sort -t $'\t' +1n -3 chr2_unsorted.bed | gzip > chr2.bed.gz
sort -t $'\t' +1n -3 chr3_unsorted.bed | gzip > chr3.bed.gz
sort -t $'\t' +1n -3 chr4_unsorted.bed | gzip > chr4.bed.gz
sort -t $'\t' +1n -3 chr5_unsorted.bed | gzip > chr5.bed.gz
sort -t $'\t' +1n -3 chr6_unsorted.bed | gzip > chr6.bed.gz
sort -t $'\t' +1n -3 chr7_unsorted.bed | gzip > chr7.bed.gz
sort -t $'\t' +1n -3 chr8_unsorted.bed | gzip > chr8.bed.gz
sort -t $'\t' +1n -3 chr9_unsorted.bed | gzip > chr9.bed.gz
sort -t $'\t' +1n -3 chr10_unsorted.bed | gzip > chr10.bed.gz
sort -t $'\t' +1n -3 chr11_unsorted.bed | gzip > chr11.bed.gz
sort -t $'\t' +1n -3 chr12_unsorted.bed | gzip > chr12.bed.gz
sort -t $'\t' +1n -3 chr13_unsorted.bed | gzip > chr13.bed.gz
sort -t $'\t' +1n -3 chr14_unsorted.bed | gzip > chr14.bed.gz
sort -t $'\t' +1n -3 chr15_unsorted.bed | gzip > chr15.bed.gz
sort -t $'\t' +1n -3 chr16_unsorted.bed | gzip > chr16.bed.gz
sort -t $'\t' +1n -3 chr17_unsorted.bed | gzip > chr17.bed.gz
sort -t $'\t' +1n -3 chr18_unsorted.bed | gzip > chr18.bed.gz
sort -t $'\t' +1n -3 chr19_unsorted.bed | gzip > chr19.bed.gz
sort -t $'\t' +1n -3 chr20_unsorted.bed | gzip > chr20.bed.gz
sort -t $'\t' +1n -3 chr21_unsorted.bed | gzip > chr21.bed.gz
sort -t $'\t' +1n -3 chr22_unsorted.bed | gzip > chr22.bed.gz
sort -t $'\t' +1n -3 chrX_unsorted.bed | gzip > chrX.bed.gz
sort -t $'\t' +1n -3 chrY_unsorted.bed | gzip > chrY.bed.gz
sort -t $'\t' +1n -3 chrM_unsorted.bed | gzip > chrM.bed.gz

# 4) remove the *_unsorted.bed files
rm *_unsorted.bed 

# 5) Merge all bed in one and add "chr" and compress
zcat chr1.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' > hg19_editingSites_darned.bed
zcat chr2.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' >> hg19_editingSites_darned.bed
zcat chr3.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' >> hg19_editingSites_darned.bed
zcat chr4.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' >> hg19_editingSites_darned.bed
zcat chr5.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' >> hg19_editingSites_darned.bed
zcat chr6.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' >> hg19_editingSites_darned.bed
zcat chr7.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' >> hg19_editingSites_darned.bed
zcat chr8.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' >> hg19_editingSites_darned.bed
zcat chr9.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' >> hg19_editingSites_darned.bed
zcat chr10.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' >> hg19_editingSites_darned.bed
zcat chr11.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' >> hg19_editingSites_darned.bed
zcat chr12.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' >> hg19_editingSites_darned.bed
zcat chr13.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' >> hg19_editingSites_darned.bed
zcat chr14.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' >> hg19_editingSites_darned.bed
zcat chr15.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' >> hg19_editingSites_darned.bed
zcat chr16.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' >> hg19_editingSites_darned.bed
zcat chr17.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' >> hg19_editingSites_darned.bed
zcat chr18.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' >> hg19_editingSites_darned.bed
zcat chr19.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' >> hg19_editingSites_darned.bed
zcat chr20.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' >> hg19_editingSites_darned.bed
zcat chr21.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' >> hg19_editingSites_darned.bed
zcat chr22.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' >> hg19_editingSites_darned.bed
zcat chrX.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' >> hg19_editingSites_darned.bed
zcat chrY.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' >> hg19_editingSites_darned.bed
zcat chrM.bed.gz | awk 'BEGIN{FS="\t";OFS="\t"}{$1="chr"$1; print}' >> hg19_editingSites_darned.bed


gzip hg19_editingSites_darned.bed

cp hg19_editingSites_darned.bed.gz ../hg19_editingSites_darned.bed.gz

#hg38 ####################################################

#https://academic.oup.com/bib/article/18/6/993/2562884#supplementary-data
#The known editing sites were obtained from DARNED [19] and RADAR [20]. Both databases use hg19, which was converted to GRCh38 using CrossMap [27]
#http://crossmap.sourceforge.net/

cd /media/sequentia/synology_office/Rosa/RNAseq/data/hg38/DARNED

wget http://hgdownload.soe.ucsc.edu/goldenPath/hg19/liftOver/hg19ToHg38.over.chain.gz

CrossMap.py bed hg19ToHg38.over.chain.gz /media/sequentia/synology_office/Rosa/RNAseq/data/hg19/hg19_editingSites_darned.bed.gz hg38_editingSites_darned.bed

gzip hg38_editingSites_darned.bed

cp hg38_editingSites_darned.bed.gz ../hg38_editingSites_darned.bed.gz

# ******************************************
# 3. RNA editing sites - RADAR
# https://github.com/aradenbaugh/radia/tree/master/data/hg19/radar
# ******************************************

#hg19 ####################################################

cd /media/sequentia/synology_office/Rosa/RNAseq/data/hg19/RADAR

wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chr1.bed.gz
wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chr2.bed.gz 
wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chr3.bed.gz 
wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chr4.bed.gz 
wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chr5.bed.gz 
wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chr6.bed.gz 
wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chr7.bed.gz 
wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chr8.bed.gz 
wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chr9.bed.gz 
wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chr10.bed.gz 
wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chr11.bed.gz 
wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chr12.bed.gz 
wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chr13.bed.gz 
wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chr14.bed.gz 
wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chr15.bed.gz 
wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chr16.bed.gz 
wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chr17.bed.gz 
wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chr18.bed.gz 
wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chr19.bed.gz 
wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chr20.bed.gz 
wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chr21.bed.gz 
wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chr22.bed.gz 
wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chrX.bed.gz 
wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chrY.bed.gz 
wget https://github.com/aradenbaugh/radia/raw/master/data/hg19/radar/chrM.bed.gz 

# 5) Merge all bed in one and add "chr" and compress

zcat chr1.bed.gz  > hg19_editingSites_radar.bed
zcat chr2.bed.gz  >> hg19_editingSites_radar.bed
zcat chr3.bed.gz  >> hg19_editingSites_radar.bed
zcat chr4.bed.gz  >> hg19_editingSites_radar.bed
zcat chr5.bed.gz  >> hg19_editingSites_radar.bed
zcat chr6.bed.gz  >> hg19_editingSites_radar.bed
zcat chr7.bed.gz  >> hg19_editingSites_radar.bed
zcat chr8.bed.gz  >> hg19_editingSites_radar.bed
zcat chr9.bed.gz  >> hg19_editingSites_radar.bed
zcat chr10.bed.gz  >> hg19_editingSites_radar.bed
zcat chr11.bed.gz  >> hg19_editingSites_radar.bed
zcat chr12.bed.gz  >> hg19_editingSites_radar.bed
zcat chr13.bed.gz  >> hg19_editingSites_radar.bed
zcat chr14.bed.gz  >> hg19_editingSites_radar.bed
zcat chr15.bed.gz  >> hg19_editingSites_radar.bed
zcat chr16.bed.gz  >> hg19_editingSites_radar.bed
zcat chr17.bed.gz  >> hg19_editingSites_radar.bed
zcat chr18.bed.gz  >> hg19_editingSites_radar.bed
zcat chr19.bed.gz  >> hg19_editingSites_radar.bed
zcat chr20.bed.gz  >> hg19_editingSites_radar.bed
zcat chr21.bed.gz  >> hg19_editingSites_radar.bed
zcat chr22.bed.gz  >> hg19_editingSites_radar.bed
zcat chrX.bed.gz  >> hg19_editingSites_radar.bed
zcat chrY.bed.gz  >> hg19_editingSites_radar.bed
zcat chrM.bed.gz  >> hg19_editingSites_radar.bed

gzip hg19_editingSites_radar.bed

cp hg19_editingSites_radar.bed.gz ../hg19_editingSites_radar.bed.gz



#hg38 ####################################################

#https://academic.oup.com/bib/article/18/6/993/2562884#supplementary-data
#The known editing sites were obtained from DARNED [19] and RADAR [20]. Both databases use hg19, which was converted to GRCh38 using CrossMap [27]
#http://crossmap.sourceforge.net/

cd /media/sequentia/synology_office/Rosa/RNAseq/data/hg38/RADAR

wget http://hgdownload.soe.ucsc.edu/goldenPath/hg19/liftOver/hg19ToHg38.over.chain.gz

CrossMap.py bed hg19ToHg38.over.chain.gz /media/sequentia/synology_office/Rosa/RNAseq/data/hg19/hg19_editingSites_radar.bed.gz hg38_editingSites_radar.bed

gzip hg38_editingSites_radar.bed

cp hg38_editingSites_radar.bed.gz ../hg38_editingSites_radar.bed.gz

# ******************************************
# 4. RepeatMasker
#This track was created by using Arian Smit's RepeatMasker program, which screens DNA sequences for interspersed repeats and low complexity DNA sequences. 
# ******************************************

https://genome.ucsc.edu/cgi-bin/hgTables?hgsid=1001028997_nACnozFrolGli566QipslYSxxKi5&clade=mammal&org=&db=hg38&hgta_group=rep&hgta_track=rmsk&hgta_table=rmsk&hgta_regionType=genome&position=&hgta_outputType=selectedFields&hgta_outFileName=hg19_RepeatMasker.txt.gz

cd /media/sequentia/synology_office/Rosa/RNAseq/data/hg19/RepeatMasker

#assembly: hg19
#group: Repeats
#track: RepeatMasker
#output format: BED
#output file: hg19_RepeatMasker.bed.gz

#assembly: hg19
#group: Repeats
#track: RepeatMasker
#output format: all fields
#output file: hg19_RepeatMasker.txt.gz

cd /media/sequentia/synology_office/Rosa/RNAseq/data/hg38/RepeatMasker

#assembly: hg38
#group: Repeats
#track: RepeatMasker
#output format: BED
#output file: hg38_RepeatMasker.bed.gz

#assembly: hg38
#group: Repeats
#track: RepeatMasker
#output format: all fields
#output file: hg38_RepeatMasker.txt.gz

# ******************************************
# 5. genomicSuperDups
# ******************************************

cp /media/sequentia/synology_office/BREDA/annovar_2019Oct24/annovar/humandb/hg19_genomicSuperDups.txt /media/sequentia/synology_office/Rosa/RNAseq/data/hg19/annotation/