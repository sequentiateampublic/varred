#!/usr/bin/env python3
"""Command-line interface for calling SNV Joint Calling."""
import sourcedefender
import logging
from varlib import commands

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format="%(message)s")
    args = commands.parse_args()
    args.func(args)
