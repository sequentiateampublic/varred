# varRED

- Installation: Go to Number [#1](#1-installation)
- Usage [#2](#2-usage):
	- Complete pipeline in batch [#2.1](#21-complete-pipeline-in-batch)
	- Step-by-step analysis [#2.2](#22-step-by-step-analysis):
        - WES calling [#2.2.1](#221-wes-calling)
        - RNA-seq calling [#2.2.2](#222-rna-seq-calling)
        - Joint variant calling [#2.2.3](#223-joint-variant-calling)
	- Other tools [#3](#3-other-tools):
        - Benchmark [#3.1](#31-benchmark)

## 1.- Installation

### 1.1.- Pre-requisites

The following softwares have to be properly installed:
- Python 3.5 or later and its following packages:
	- [Pandas](http://pandas.pydata.org)
	- [Biopython](http://biopython.org/wiki/Main_Page)
	- [pybedtools](https://daler.github.io/pybedtools/index.html)
- BEDtools from https://github.com/arq5x/bedtools2
- samtools from http://samtools.sourceforge.net/
- Picard Tools from http://broadinstitute.github.io/picard/
- bcftools from http://samtools.github.io/bcftools/bcftools.html
- vcftools from http://vcftools.sourceforge.net/
- Annovar with the genomicSuperDups database from https://annovar.openbioinformatics.org/en/latest/ . genomicSuperDups database should be installed at data/databases
- Sentieon Genomics pipeline tools (License required) from https://support.sentieon.com/manual/
- STAR from https://github.com/alexdobin/STAR
- hap.py using Docker from https://github.com/Illumina/hap.py

### 1.2.- Download databases required for calling

- dbsnp from https://ftp.ncbi.nlm.nih.gov/snp/latest_release/VCF/GCF_000001405.38.gz
- db1000G from ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/technical/reference/GRCh38_reference_genome/other_mapping_resources/ALL.wgs.1000G_phase3.GRCh38.ncbi_remapper.20150424.shapeit2_indels.vcf.gz
- dbmills from ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/technical/reference/GRCh38_reference_genome/other_mapping_resources/Mills_and_1000G_gold_standard.indels.b38.primary_assembly.vcf.gz

### 1.3.- Installation

1. Download repository
```shell
git clone git@gitlab.com:sequentiateampublic/varred
```

## 2.- Usage

### 2.1.- Complete pipeline in batch

#### 2.1.1.- Usage
```python
usage: varRED.py batch [-h] -f FASTA [-o FILENAME] [-O DIRECTORY] [-g GENOME]
                       [-t CPUs] [-SL FILE] [-SP DIRECTORY] --dbsnp dbsnp
                       --db_1000G db_1000G --db_mills db_mills -gF DIRECTORY
                       [-pJ FILENAME] [-pA DIRECTORY] [-pAdb DIRECTORY]
                       [-mdp INT] [-msb INT] [-rm]
                       WES_fastq_forward WES_fastq_reverse RNA_fastq_forward
                       RNA_fastq_reverse
```

#### 2.1.2.- Arguments

##### Mandatory Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| WES_fastq_forward | - | Path to the DNA fastq forward file. |
| WES_fastq_reverse | - | Path to the DNA fastq reverse file. |
| RNA_fastq_forward | - | Path to the RNA fastq forward file. |
| RNA_fastq_reverse | - | Path to the RNA fastq reverse file. |
| -f FASTA, --fasta FASTA | - | Reference genome FASTA file to be used. |
| -gF DIRECTORY, --genomeDir DIRECTORY | - | Path to the directory where genome files of STAR are stored. |
| --dbsnp dbsnp | - | Location of the Single Nucleotide Polymorphism database (dbSNP) used to label known variants. Only one file is supported. |
| --db_1000G db_1000G | - | Location of the 1000G high-confidence sites used as a set of known sites. |
| --db_mills db_mills | - | Location of the Mills and 1000G gold standard indels used as a set of known sites. |

##### Optional Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| -h, --help   | -   | show this help message and exit. |
| -o FILENAME, --output FILENAME | batch_output | Output filename. Empty for default. |
| -O DIRECTORY, --output-dir DIRECTORY | batch_output | Output directory name. Empty for default. |
| -g {hg19,hg38}, --genome {hg19,hg38} | hg19 | Genome assembly. |
| -t CPUs, --threads CPUs | Maximum available at your computer | Number of CPUs. |
| -SL FILE, --SENTIEON-LICENSE FILE | ~/programs/Sequentia_Biotech_SL_cluster.lic | Path to SENTIEON license file. |
| -SP DIRECTORY, --SENTIEON-INSTALL-DIR DIRECTORY | ~/programs/sentieon-genomics-202010.01 | Path to SENTIEON directory. |
| -pJ FILENAME, --picardJar FILENAME | scripts/picard.jar | Path to Picard program JAR file. |
| -pA DIRECTORY, --annovarPath DIRECTORY | ~/programs/annovar | Path to the folder containing ANNOVAR program. |
| -pAdb DIRECTORY, --annovarDBPath DIRECTORY | data/databases | Path to the folder containing databases to be used by ANNOVAR program. |

#### 2.1.3.- Outputs

- Final VCF with short variants.
- WES gVCF
- WES aligment BAM file
- RNA gVCF
- RNA aligment BAM file

### 2.2.- Step-by-step analysis

#### 2.2.1.- WES calling

##### 2.2.1.1.- Usage

```python
usage: varRED.py WEScalling [-h] -f FASTA [-o FILENAME] [-O DIRECTORY]
                            [-g GENOME] [-t CPUs] [-SL FILE] [-SP DIRECTORY]
                            --dbsnp dbsnp --db_1000G db_1000G --db_mills
                            db_mills
                            fastq_forward fastq_reverse
```
##### 2.2.1.2.- Arguments

###### Mandatory Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| fastq_forward | - | Path to the DNA fastq forward file. |
| fastq_reverse | - | Path to the DNA fastq reverse file. |
| -f FASTA, --fasta FASTA | - | Reference genome FASTA file to be used. |
| --dbsnp dbsnp | - | Location of the Single Nucleotide Polymorphism database (dbSNP) used to label known variants. Only one file is supported. |
| --db_1000G db_1000G | - | Location of the 1000G high-confidence sites used as a set of known sites. |
| --db_mills db_mills | - | Location of the Mills and 1000G gold standard indels used as a set of known sites. |

###### Optional Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| -h, --help   | -   | show this help message and exit. |
| -o FILENAME, --output FILENAME | WEScalling_output | Output filename. Empty for default. |
| -O DIRECTORY, --output-dir DIRECTORY | WEScalling_output | Output directory name. Empty for default. |
| -g {hg19,hg38}, --genome {hg19,hg38} | hg19 | Genome assembly. |
| -t CPUs, --threads CPUs | Maximum available at your computer | Number of CPUs. |
| -SL FILE, --SENTIEON-LICENSE FILE | ~/programs/Sequentia_Biotech_SL_cluster.lic | Path to SENTIEON license file. |
| -SP DIRECTORY, --SENTIEON-INSTALL-DIR DIRECTORY | ~/programs/sentieon-genomics-202010.01 | Path to SENTIEON directory. |

#### 2.2.2.- RNA-seq calling

##### 2.2.2.1.- Usage

```python
usage: varRED.py RNAcalling [-h] -f FASTA [-o FILENAME] [-O DIRECTORY]
                            [-g GENOME] [-t CPUs] [-SL FILE] [-SP DIRECTORY]
                            --dbsnp dbsnp --db_1000G db_1000G --db_mills
                            db_mills -gF DIRECTORY [-pJ FILENAME]
                            fastq_forward fastq_reverse
```

##### 2.2.2.2.- Arguments

###### Mandatory Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| fastq_forward | - | Path to the RNA fastq forward file. |
| fastq_reverse | - | Path to the RNA fastq reverse file. |
| -gF DIRECTORY, --genomeDir DIRECTORY | - | Path to the directory where genome files of STAR are stored. |
| -f FASTA, --fasta FASTA | - | Reference genome FASTA file to be used. |
| --dbsnp dbsnp | - | Location of the Single Nucleotide Polymorphism database (dbSNP) used to label known variants. Only one file is supported. |
| --db_1000G db_1000G | - | Location of the 1000G high-confidence sites used as a set of known sites. |
| --db_mills db_mills | - | Location of the Mills and 1000G gold standard indels used as a set of known sites. |

###### Optional Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| -h, --help   | -   | show this help message and exit. |
| -o FILENAME, --output FILENAME | RNAcalling_output | Output filename. Empty for default. |
| -O DIRECTORY, --output-dir DIRECTORY | RNAcalling_output | Output directory name. Empty for default. |
| -g {hg19,hg38}, --genome {hg19,hg38} | hg19 | Genome assembly. |
| -t CPUs, --threads CPUs | Maximum available at your computer | Number of CPUs. |
| -SL FILE, --SENTIEON-LICENSE FILE | ~/programs/Sequentia_Biotech_SL_cluster.lic | Path to SENTIEON license file. |
| -SP DIRECTORY, --SENTIEON-INSTALL-DIR DIRECTORY | ~/programs/sentieon-genomics-202010.01 | Path to SENTIEON directory. |
| -pJ FILENAME, --picardJar FILENAME | scripts/picard.jar | Path to Picard program JAR file. |

#### 2.2.3.- Joint variant calling

##### 2.2.3.1.- Usage

```python
usage: varRED.py joint [-h] -f FASTA [-o FILENAME] [-O DIRECTORY] [-g GENOME]
                       --dbsnp dbsnp [-ec INT] [-cc INT] [-SL FILE]
                       [-SP DIRECTORY] [-t CPUs] [-pA DIRECTORY]
                       [-pAdb DIRECTORY] [-mdp INT] [-msb INT]
                       DNA_gVCF RNA_gVCF
```

##### 2.2.3.2.- Arguments

###### Mandatory Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| DNA_gVCF | -   | PATH to DNA gVCF. |
| RNA_gVCF | -   | PATH to RNA gVCF. |
| -f FASTA, --fasta FASTA | - | Reference genome FASTA file to be used. |
| --dbsnp dbsnp | - | Location of the Single Nucleotide Polymorphism database (dbSNP) used to label known variants. Only one file is supported. |

###### Optional Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| -h, --help   | -   | show this help message and exit. |
| -o FILENAME, --output FILENAME | joint_output | Output filename. Empty for default. |
| -O DIRECTORY, --output-dir DIRECTORY | joint_output | Output directory name. Empty for default. |
| -g {hg19,hg38}, --genome {hg19,hg38} | hg19 | Genome assembly. |
| -ec INT, --emit_conf INT | 20 |Determine the threshold of variant quality to emit a variant. |
| -cc INT, --call_conf INT | 20 |Determine the threshold of variant quality to call a variant. |
| -SL FILE, --SENTIEON-LICENSE FILE | ~/programs/Sequentia_Biotech_SL_cluster.lic | Path to SENTIEON license file. |
| -SP DIRECTORY, --SENTIEON-INSTALL-DIR DIRECTORY | ~/programs/sentieon-genomics-202010.01 | Path to SENTIEON directory. |
| -t CPUs, --threads CPUs | Maximum available at your computer | Number of CPUs. |
| -pA DIRECTORY, --annovarPath DIRECTORY | ~/programs/annovar | Path to the folder containing ANNOVAR program. |
| -pAdb DIRECTORY, --annovarDBPath DIRECTORY | data/databases | Path to the folder containing databases to be used by ANNOVAR program. |
| -mdp INT, --minDP INT | 6 | Minimum read depth (DP) threshold. The minimum read depth (DP) for both DNA and RNA data. |
| -msb INT, --minSB INT | 2 | Maximum strand bias (SB) threshold. The maximum strand bias (SB) for both DNA and RNA data. This filter is only applied for variants where the Read Count of forward (of major allele ) >= 10 and the Read Count of reverse (of major allele ) >= 10 (default 2). |

## 3.- Other tools

### 3.1.- Benchmark

Benchmark performance of the short variant calling.

#### 3.1.1.- Usage
```python
usage: varRED.py benchmark [-h] -f FASTA [-o FILENAME] [-O DIRECTORY]
                           [-tb BED] [-gd GENDER] -i IMAGE_ID [-g GENOME] -dna
                           DNA -rna RNA [-t CPUs] [-rm]
                           queryVCF truthVCF
```

#### 3.1.2.- Arguments

##### Mandatory Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| queryVCF | -   |  PATH to joint VCF of DNA and RNA (output of joint). |
| truthVCF | - |  PATH to truth VCF with the real short variant of the sample. Only the sample of intersest should be in the VCF. |
| -f FASTA, --fasta FASTA | - | Reference genome FASTA file to be used. |
| -i IMAGE_ID, --IMAGE_ID IMAGE_ID | - |  Image ID of the docker containing hap.py. |
| -dna DNA, --nameDNA DNA | - | Name of the column on the query VCF with the DNA information |
| -rna RNA, --nameRNA RNA | - | Name of the column on the query VCF with the RNA information |

##### Optional Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| -h, --help   | -   | show this help message and exit. |
| -o FILENAME, --output FILENAME | benchmark_output | Output filename. Empty for default. |
| -O DIRECTORY, --output-dir DIRECTORY | benchmark_output | Output directory name. Empty for default. |
| -g {hg19,hg38}, --genome {hg19,hg38} | hg19 | Genome assembly. |
| -tb BED, --truthBED BED | - | PATH to the BED file with the truthset confident regions |
| -gd {female,male}, --gender {female,male} | - |  Gender of the sample. |
| -t CPUs, --threads CPUs | Maximum available at your computer | Number of CPUs. |
| -rm, --remove | - | Remove intermediate files and directories. |

